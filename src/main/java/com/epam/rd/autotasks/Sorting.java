package com.epam.rd.autotasks;

import java.util.Arrays;

public class Sorting {
    public int[] sort(int[] numbers){
        if (numbers.length > 10) {
            throw new NumberFormatException();
        }
        else if (numbers.length == 0){
            throw new IllegalArgumentException();
        }

        Arrays.sort(numbers);
        return numbers;
    }

}
