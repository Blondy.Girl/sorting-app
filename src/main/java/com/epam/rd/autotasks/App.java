package com.epam.rd.autotasks;

import java.util.*;

public class App {
    public static void main(String[] args) {

        String[] numberStrs;
        System.out.println("Input up to ten arguments separated by space: ");
        while (true) {
            Scanner sc = new Scanner(System.in);
            String line = sc.nextLine();
            numberStrs = line.split(" ");
            if ( numberStrs[0].equals("")) {
                System.err.println("Enter up to ten arguments separated by space: ");
            }
            else {
                break;
            }
        }
        int[] numbers = new int[0];
        try {
            numbers = new int[numberStrs.length];
            for (int i = 0; i < numberStrs.length; i++) {
                numbers[i] = Integer.parseInt(numberStrs[i]);
            }
        } catch (NumberFormatException e) {
            System.err.println("Incorrect input");
            return;
        }
//        sorting eto object class, kotoryj umeet sortirovat
        Sorting sorting = new Sorting();
        // my vyzyvaem funkciju
        int[] result = sorting.sort(numbers);
        String str = Arrays
                .stream(result)
                .mapToObj(String::valueOf)
                .reduce((a, b) -> a.concat(" ").concat(b))
                .get();
        System.out.println(str);
    }
}







