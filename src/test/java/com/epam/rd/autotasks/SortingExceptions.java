package com.epam.rd.autotasks;

import org.junit.Test;

public class SortingExceptions {

    @Test (expected = NumberFormatException.class)
    public void testSortingNumberFormatException(){
        Sorting sortTest = new Sorting();
        sortTest.sort(new int[] {1,2,3,4,5,6,7,8,9,10,11});
    }

    @Test (expected = IllegalArgumentException.class)
    public void testSortingIllegalArgumentException(){
        Sorting sortTest = new Sorting();
        sortTest.sort(new int[]{});
    }

}
