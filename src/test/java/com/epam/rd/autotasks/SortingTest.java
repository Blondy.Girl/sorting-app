package com.epam.rd.autotasks;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class SortingTest
{
    public Sorting sorting = new Sorting();
    private int[] numbers;
    private int[] expected;

    public SortingTest(int[] numbers, int[] expected){
        this.numbers = numbers;
        this.expected = expected;
    }
    @Parameterized.Parameters
    public static Collection primeNumbers(){
        return Arrays.asList(new Object[][]{
                {new int[] {0}, new int[] {0}},
                {new int[] {1,3,2,5,4,7,6,9,8,10}, new int[]{1,2,3,4,5,6,7,8,9,10}},
        });
    }

    @Test
    public void testSort() {
        int[] result = this.sorting.sort(this.numbers);
        assertArrayEquals(this.expected, result);
        
    }

}
